package com.sinsz.pay.dto;

/**
 * 退款查询结果
 * @author chenjianbo
 */
public class RefundQueryDto {

    /**
     * 支付平台交易订单ID
     */
    private String transactionId;

    /**
     * 自定义商家订单号
     */
    private String outTradeNo;

    /**
     * 自定义商家退款单号
     */
    private String outRefundNo;

    /**
     * 支付平台退款交易单号
     */
    private String refundId;

    /**
     * 退款笔数
     */
    private int refundCount;

    /**
     * 退款状态
     */
    private String refundStatus;

    /**
     * 退款成功时间
     */
    private String refundSuccessTime;

    public RefundQueryDto() {
    }

    public RefundQueryDto(String transactionId, String outTradeNo, String outRefundNo, String refundId, int
            refundCount, String refundStatus, String refundSuccessTime) {
        this.transactionId = transactionId;
        this.outTradeNo = outTradeNo;
        this.outRefundNo = outRefundNo;
        this.refundId = refundId;
        this.refundCount = refundCount;
        this.refundStatus = refundStatus;
        this.refundSuccessTime = refundSuccessTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public int getRefundCount() {
        return refundCount;
    }

    public void setRefundCount(int refundCount) {
        this.refundCount = refundCount;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getRefundSuccessTime() {
        return refundSuccessTime;
    }

    public void setRefundSuccessTime(String refundSuccessTime) {
        this.refundSuccessTime = refundSuccessTime;
    }
}
