package com.sinsz.pay.dto;

/**
 * 退款信息
 * @author chenjianbo
 */
public class RefundDto {

    /**
     * 支付平台交易订单ID
     */
    private String transactionId;

    /**
     * 自定义商家订单号
     */
    private String outTradeNo;

    /**
     * 自定义商家退款单号
     */
    private String outRefundNo;

    /**
     * 支付平台退款交易单号
     */
    private String refundId;

    /**
     * 退款申请时间
     */
    private long applyTime;

    public RefundDto() {
    }

    public RefundDto(String transactionId, String outTradeNo, String outRefundNo, String refundId, long applyTime) {
        this.transactionId = transactionId;
        this.outTradeNo = outTradeNo;
        this.outRefundNo = outRefundNo;
        this.refundId = refundId;
        this.applyTime = applyTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getOutRefundNo() {
        return outRefundNo;
    }

    public void setOutRefundNo(String outRefundNo) {
        this.outRefundNo = outRefundNo;
    }

    public String getRefundId() {
        return refundId;
    }

    public void setRefundId(String refundId) {
        this.refundId = refundId;
    }

    public long getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(long applyTime) {
        this.applyTime = applyTime;
    }
}
