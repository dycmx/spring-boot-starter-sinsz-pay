package com.sinsz.pay.exception;

/**
 * 支付异常统一异常类
 *
 * @author chenjianbo
 */
public class PayException extends RuntimeException {

    private static final String CODE = "";

    public PayException(String message) {
        super(CODE + "" + message);
    }

    @Override
    public String toString() {
        return CODE + "" + super.getMessage();
    }

}
