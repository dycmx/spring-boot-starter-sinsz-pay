package com.sinsz.pay.factory;

import com.sinsz.pay.exception.PayException;
import com.sinsz.pay.factory.alipay.AlipayAppImpl;
import com.sinsz.pay.factory.wechat.WxPayImpl;
import com.sinsz.pay.properties.PayProperties;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付路由
 *
 * @author chenjianbo
 */
public class Route {

    private final PayProperties properties;
    private final HttpServletRequest request;

    public Route(PayProperties properties, HttpServletRequest request) {
        this.properties = properties;
        this.request = request;
    }

    public Pay pay(PayMethod channel) {
        BasePay basePay;
        switch (channel) {
            case WECHAT_APP:
            case WECHAT_PUBLIC:
                if (properties.getWxpay() == null) {
                    throw new PayException("微信支付配置未设置");
                }
                basePay = new WxPayImpl(properties, request);
                break;
            case ALIPAY_APP:
                if (properties.getAlipay() == null) {
                    throw new PayException("支付宝支付配置未设置");
                }
                basePay = new AlipayAppImpl(properties, request);
                break;
            case ALIPAY_WAP:
            default:
                throw new PayException(channel.name() + "支付方式暂未开放");
        }
        return basePay;
    }

}
